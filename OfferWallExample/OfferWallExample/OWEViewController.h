//
//  OWEViewController.h
//  OfferWallExample
//
//  Created by Sharim Chua on 13/03/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OWEViewController : UIViewController
- (IBAction)showOfferWall:(id)sender;
- (IBAction)startTimer:(id)sender;
- (IBAction)stopTimer:(id)sender;
- (IBAction)pauseTimer:(id)sender;
- (IBAction)resumeTimer:(id)sender;
- (IBAction)restartTimer:(id)sender;
@end
