//
//  OWEViewController.m
//  OfferWallExample
//
//  Created by Sharim Chua on 13/03/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OWEViewController.h"
#import "OfferWall.h"

@interface OWEViewController ()

@end

@implementation OWEViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showOfferWall:(id)sender {
    [OfferWall showOffersFromViewController:self];
}

- (IBAction)startTimer:(id)sender {
    // Start a 5 second timer
    [OfferWall startOfferWallTimer:5 viewController:self];
}

- (IBAction)stopTimer:(id)sender {
    [OfferWall stopOfferWallTimer];
}

- (IBAction)pauseTimer:(id)sender {
    [OfferWall pauseOfferWallTimer];
}

- (IBAction)resumeTimer:(id)sender {
    [OfferWall resumeOfferWallTimer];
}

- (IBAction)restartTimer:(id)sender {
    [OfferWall restartOfferWallTimer];
}
@end
