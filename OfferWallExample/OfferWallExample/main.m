//
//  main.m
//  OfferWallExample
//
//  Created by Sharim Chua on 13/03/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OWEAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OWEAppDelegate class]));
    }
}
