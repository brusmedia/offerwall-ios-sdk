//
//  OWEAppDelegate.h
//  OfferWallExample
//
//  Created by Sharim Chua on 13/03/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OWEAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
