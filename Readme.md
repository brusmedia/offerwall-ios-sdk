# Brus Media OfferWall iOS SDK
The OfferWall SDK provides a simple means of integrating your game with Brus Media offers. You may use the build in default skin for the Offer Wall or override various elements of the presentation if preferred.

## Installation Instructions

Copy the OfferWall.h and OfferWall.a files into your iOS project.
Import <OfferWall/OfferWall.h> in your App Delegate class
On your application startup, call the OfferWall SDK initialisation method with your App API Key provided by Brus Media

	- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
		// Initialise the Offer Wall
		[OfferWall initialiseOfferWallWithKey:@"YOUR_API_KEY"];
	}

The OfferWall SDK is dependant on several frameworks which will need to be included in your XCode Project. These include:

- SystemConfiguration.framework
- AdSupport.framework

Finally ensure you add the "-ObjC" as a flag under "Other Linker Flags" in your project build settings.

## Displaying the Offer Wall

### Directly activating the Offer Wall

In your View Controllers you can present the Offer Wall by calling the showOffersFromViewController: method and passing in a reference to the active View Controller.

	[OfferWall showOffersFromViewController:self];

### Setting the Offer Wall to display on a timer

In some cases you may prefer to display the Offer Wall on a set interval (while the user is playing your game for example). This can be initiated with the startOfferWallTimer:viewController method, passing in an interval (in seconds) and a reference to the active View Controller.

	// Initiates a 5 minute timer from the current view controller
	[OfferWall startOfferWallTimer:300.0f viewController:self];

The timer may also be stopped if manually by the stopOfferWallTimer method. Timers are automatically stopped should the timer event fire and the view controller assigned is not active or visible.

	[OfferWall stopOfferWallTimer];

Additional control of the timer can be achieved through pausing, resuming and restarting the timer with each of the following methods.

	// Pause a timer
	[OfferWall pauseOfferWallTimer];
	// Resume a paused timer
	[OfferWall resumeOfferWallTimer];
	// Restart a timer to the original interval set
	[OfferWall restartOfferWallTimer];

## Further Configuration

By default there is not additional configuration required to display the Offer Wall beyond setting the app API key. A default skin with assets is already included in the library, though if necessary the following attributes of the Offer Wall may be customised.

- Background Colour (The colour of the background view behind the Offer Wall and Loading View)
- Status Bar Visibility (Whether the status bar will be visible when displaying the Offer Wall)
- Close Button Image (The image used for the basic close button that dismisses the Offer Wall)
- Cell View (A NIB that contains the Cell View for the Offer Wall table. This view should be an instance of OWOfferCell with IBOutlets mapped in Interface Builder)
- Horizontal Padding (Fixed padding on left and right of the Offer Wall in points)
- Custom Header View (A NIB that contains a View for the Offer Wall. This view should be an instance of OWOfferTableHeader with IBOutlets mapped in Interface Builder)
- Loading View (A NIB that contains an interstitial View that is displayed in place of the Offer Wall if Offers have not been loaded yet)