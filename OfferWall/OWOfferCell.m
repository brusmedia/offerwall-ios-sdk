//
//  OWOfferCell.m
//  OfferWall
//
//  Created by Sharim Chua on 3/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OfferWall.h"

@implementation OWOfferCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
