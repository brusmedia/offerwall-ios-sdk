//
//  OWLoadingViewController.m
//  OfferWall
//
//  Created by Sharim Chua on 13/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OWLoadingViewController.h"
#import "OWOfferDataSource.h"
#import "OWStaticAssets.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>

#define kLoadingTimeout 10

@interface OWLoadingViewController ()
@property (weak, nonatomic) OWConfiguration *configuration;
@property (strong, nonatomic) UIView *loadingView;
@property (strong, nonatomic) AFNetworkReachabilityManager *reachabilityManager;
- (void) loadingCompleted;
@end

@implementation OWLoadingViewController

-(id)initWithConfiguration:(OWConfiguration *)configuration {
    self = [super init];
    if (self) {
        self.configuration = configuration;
        self.reachabilityManager = [AFNetworkReachabilityManager managerForDomain:@"offerwallapi.brusmedia.com"];
        [self.reachabilityManager startMonitoring];
    }
    return self;
}

-(BOOL)prefersStatusBarHidden {
    return self.configuration.statusBarVisibility;
}

-(UIView *)setupHeaderView {
    UILabel *headerTitle = [[UILabel alloc] init];
    headerTitle.text = @"More Free Games";
    headerTitle.textColor = [UIColor whiteColor];
    headerTitle.backgroundColor = [UIColor clearColor];
    [headerTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:headerTitle];
    UIButton *closeButton = [[UIButton alloc] init];
    [closeButton setImage:[OWStaticAssets defaultCloseButtonImage] forState:UIControlStateNormal];
    [closeButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [closeButton addTarget:self action:@selector(dismissOfferWall) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:closeButton];
    NSArray *verticalTitleConstraints = [NSLayoutConstraint
                                         constraintsWithVisualFormat:@"V:|[title]|"
                                         options:NSLayoutFormatDirectionLeadingToTrailing
                                         metrics:nil
                                         views:@{@"title": headerTitle}];
    NSArray *verticalCloseButtonContraints = [NSLayoutConstraint
                                              constraintsWithVisualFormat:@"V:|[closeButton]|"
                                              options:NSLayoutFormatDirectionLeadingToTrailing
                                              metrics:nil
                                              views:@{@"closeButton": closeButton}];
    NSArray *horizontalContraints = [NSLayoutConstraint
                                     constraintsWithVisualFormat:@"|-20-[title]-10@1-[closeButton]-0-|"
                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                     metrics:nil
                                     views:@{@"closeButton": closeButton, @"title": headerTitle}];
    [headerView addConstraints:verticalCloseButtonContraints];
    [headerView addConstraints:verticalTitleConstraints];
    [headerView addConstraints:horizontalContraints];
    headerView.backgroundColor = [UIColor colorWithPatternImage:[OWStaticAssets defaultHeaderGradientImage]];
    [headerView setTranslatesAutoresizingMaskIntoConstraints:NO];
    return headerView;
}

-(void)setupLoadingView {
    self.loadingView = [[UIView alloc] init];
    if (self.configuration.backgroundColour) {
        self.loadingView.backgroundColor = self.configuration.backgroundColour;
    } else {
        self.loadingView.backgroundColor = [UIColor blackColor];
    }
    self.loadingView.opaque = NO;
    
    UIView *headerView = [self setupHeaderView];
    [self.loadingView addSubview:headerView];
    
    UIView *loadingView = nil;
    if (self.configuration.loadingViewNib) {
        loadingView = [[self.configuration.loadingViewNib instantiateWithOwner:self options:nil] lastObject];
    } else {
        UILabel *loadingLabel = [[UILabel alloc] init];
        if (self.reachabilityManager.reachable) {
            loadingLabel.text = @"Loading Games...";
        } else {
            loadingLabel.text = @"No network connection";
        }
        [self.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            if (status == AFNetworkReachabilityStatusNotReachable) {
                loadingLabel.text = @"No network connection";
            } else {
                loadingLabel.text = @"Loading Games...";
            }
        }];

        loadingLabel.textColor = [UIColor whiteColor];
        loadingLabel.backgroundColor = [UIColor clearColor];
        loadingLabel.font = [UIFont systemFontOfSize:20.0];
        loadingView = loadingLabel;
    }
    [loadingView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.loadingView addSubview:loadingView];    
    
    NSArray *verticalContraints = [NSLayoutConstraint
                                              constraintsWithVisualFormat:@"V:|-20-[headerView]-[loadingView]|"
                                              options:NSLayoutFormatDirectionLeadingToTrailing
                                              metrics:nil
                                              views:@{@"headerView": headerView, @"loadingView": loadingView}];
    NSArray *horizontalContraints = [NSLayoutConstraint
                                     constraintsWithVisualFormat:@"|-[headerView]-|"
                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                     metrics:nil
                                     views:@{@"headerView": headerView}];
    [self.loadingView addConstraints:verticalContraints];
    [self.loadingView addConstraints:horizontalContraints];
    [self.loadingView addConstraint:[NSLayoutConstraint constraintWithItem:loadingView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.loadingView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
}

-(void)loadView {
    [self setupLoadingView];
    self.view = self.loadingView;
}

-(void)dismissOfferWall {
    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)loadingCompleted {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadingCompleted) name:kOffersLoadedEvent object:nil];
    [NSTimer scheduledTimerWithTimeInterval:kLoadingTimeout target:self selector:@selector(dismissOfferWall) userInfo:nil repeats:NO];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
