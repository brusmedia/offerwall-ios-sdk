//
//  OWOfferDataSource.h
//  OfferWall
//
//  Created by Sharim Chua on 3/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OWConfiguration.h"

#define kOffersLoadedEvent @"OffersLoaded"

@protocol OWOfferDataSourceDelegate
-(void)hasSelectedOffer;
@end

@interface OWOfferDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) UINib *cellViewNib;
@property (weak, nonatomic) id<OWOfferDataSourceDelegate> delegate;
-(id)initWithConfiguration: (OWConfiguration *) configuration;
-(void)loadData;
-(BOOL)dataLoadComplete;
@end
