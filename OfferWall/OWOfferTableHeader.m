//
//  OWOfferTableHeader.m
//  OfferWall
//
//  Created by Sharim Chua on 12/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OfferWall.h"

@implementation OWOfferTableHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
