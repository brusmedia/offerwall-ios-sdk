//
//  OWStaticAssets.m
//  OfferWall
//
//  Created by Sharim Chua on 26/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OWStaticAssets.h"

@interface OWStaticAssets ()
+(BOOL) isRetinaDisplay;
@end

@implementation OWStaticAssets

+(BOOL)isRetinaDisplay {
    return [[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && [UIScreen mainScreen].scale == 2.0;
}

+(UIImage *)defaultCloseButtonImage {
    extern unsigned char __defaultCloseButton_png[];
    extern unsigned int __defaultCloseButton_png_len;
    extern unsigned char __defaultCloseButton_2x_png[];
    extern unsigned int __defaultCloseButton_2x_png_len;
    NSData *data = nil;
    if ([OWStaticAssets isRetinaDisplay]) {
        data = [NSData dataWithBytes:__defaultCloseButton_2x_png length:__defaultCloseButton_2x_png_len];
        return [UIImage imageWithData:data scale:2.0];
    } else {
        data = [NSData dataWithBytes:__defaultCloseButton_png length:__defaultCloseButton_png_len];
        return [UIImage imageWithData:data];
    }
}

+(UIImage *)defaultHeaderGradientImage {
    extern unsigned char __defaultHeaderGradient_png[];
    extern unsigned int __defaultHeaderGradient_png_len;
    extern unsigned char __defaultHeaderGradient_2x_png[];
    extern unsigned int __defaultHeaderGradient_2x_png_len;
    NSData *data = nil;
    if ([OWStaticAssets isRetinaDisplay]) {
        data = [NSData dataWithBytes:__defaultHeaderGradient_2x_png length:__defaultHeaderGradient_2x_png_len];
        return [UIImage imageWithData:data scale:2.0];
    } else {
        data = [NSData dataWithBytes:__defaultHeaderGradient_png length:__defaultHeaderGradient_png_len];
        return [UIImage imageWithData:data];
    }
}

+(UIImage *)defaultCellGradientImage {
    extern unsigned char __defaultCellGradient_png[];
    extern unsigned int __defaultCellGradient_png_len;
    extern unsigned char __defaultCellGradient_2x_png[];
    extern unsigned int __defaultCellGradient_2x_png_len;
    NSData *data = nil;
    if ([OWStaticAssets isRetinaDisplay]) {
        data = [NSData dataWithBytes:__defaultCellGradient_2x_png length:__defaultCellGradient_2x_png_len];
        return [UIImage imageWithData:data scale:2.0];
    } else {
        data = [NSData dataWithBytes:__defaultCellGradient_png length:__defaultCellGradient_png_len];
        return [UIImage imageWithData:data];
    }
    
}

+(UIImage *)defaultPriceButtonImage {
    extern unsigned char __defaultPriceButton_png[];
    extern unsigned int __defaultPriceButton_png_len;
    extern unsigned char __defaultPriceButton_2x_png[];
    extern unsigned int __defaultPriceButton_2x_png_len;
    NSData *data = nil;
    if ([OWStaticAssets isRetinaDisplay]) {
        data = [NSData dataWithBytes:__defaultPriceButton_2x_png length:__defaultPriceButton_2x_png_len];
        return [UIImage imageWithData:data scale:2.0];
    } else {
        data = [NSData dataWithBytes:__defaultPriceButton_png length:__defaultPriceButton_png_len];
        return [UIImage imageWithData:data];
    }
}

@end
