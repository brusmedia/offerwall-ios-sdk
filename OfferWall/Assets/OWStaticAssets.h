//
//  OWStaticAssets.h
//  OfferWall
//
//  Created by Sharim Chua on 26/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OWStaticAssets : NSObject
+ (UIImage *) defaultCloseButtonImage;
+ (UIImage *) defaultHeaderGradientImage;
+ (UIImage *) defaultCellGradientImage;
+ (UIImage *) defaultPriceButtonImage;
@end
