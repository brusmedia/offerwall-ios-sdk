//
//  OWIdentificationService.m
//  OfferWall
//
//  Created by Sharim Chua on 5/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OWIdentificationService.h"
#import <AdSupport/AdSupport.h>

#define kGeoLocationTimeOut 5
#define kIPad @"iPad"
#define kIPhone @"iPhone"

@implementation OWIdentificationService

+(OWIdentificationService *)sharedInstance {
    static OWIdentificationService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[OWIdentificationService alloc] init];
    });
    return sharedInstance;
}

-(NSString *)getDeviceTypeName {
    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType isEqualToString:@"iPad"]) {
        return kIPad;
#ifdef DEBUG
    } else if ([deviceType isEqualToString:@"iPad Simulator"]) {
        return kIPad;
#endif
    } else {
        return kIPhone;
    }
}

-(NSString *)getAdvertisingIdentifier {
    return [[ASIdentifierManager sharedManager] advertisingIdentifier].UUIDString;
}

-(NSString *)getCountryCode {
    return [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
}

@end
