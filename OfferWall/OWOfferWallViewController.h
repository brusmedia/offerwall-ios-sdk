//
//  OfferWallViewController.h
//  OfferWall
//
//  Created by Sharim Chua on 6/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OWOfferDataSource.h"
#import "OWConfiguration.h"

@interface OWOfferWallViewController : UIViewController <OWOfferDataSourceDelegate>
-(id)initWithTableManager:(OWOfferDataSource *)tableManager
            Configuration:(OWConfiguration *)configuration;
@end
