//
//  OWConfiguration.h
//  OfferWall
//
//  Created by Sharim Chua on 12/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OWConfiguration : NSObject
@property (strong, nonatomic) NSString *apiKey;
@property (strong, nonatomic) UINib *offerCellViewNib;
@property (strong, nonatomic) UINib *loadingViewNib;
@property (strong, nonatomic) UINib *headerViewNib;
@property (strong, nonatomic) UIColor *backgroundColour;
@property (assign, nonatomic) BOOL statusBarVisibility;
@property (strong, nonatomic) UIImage *closeImage;
@property (assign, nonatomic) int horizontalPadding;
@end
