//
//  OWHasOffersService.h
//  OfferWall
//
//  Created by Sharim Chua on 5/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OWHasOffersService : NSObject
+(OWHasOffersService *)sharedInstance;

-(void)retrieveAllOffersForAPIKey:(NSString *)apiKey
                          Country:(NSString *)countryCode
                           Device:(NSString *)device
            AdvertisingIdentifier:(NSString *)ifa
                          Success:(void (^)(NSArray *results)) successCallback
                          Failure:(void (^)(NSError *error)) failureCallback;

@end
