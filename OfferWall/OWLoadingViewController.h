//
//  OWLoadingViewController.h
//  OfferWall
//
//  Created by Sharim Chua on 13/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OWConfiguration.h"

@interface OWLoadingViewController : UIViewController
- (id) initWithConfiguration: (OWConfiguration *)configuration;
@end
