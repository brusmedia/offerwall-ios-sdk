//
//  OWOfferRepository.m
//  OfferWall
//
//  Created by Sharim Chua on 5/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OWOffer.h"
#import "OWOfferRepository.h"
#import "OWIdentificationService.h"
#import "OWHasOffersService.h"

@implementation OWOfferRepository

-(void)retrieveOffersForApiKey:(NSString *)apiKey Success:(void (^)(NSArray *))successCallback Failure:(void (^)(NSError *))failureCallback {
    OWHasOffersService *hasOffers = [OWHasOffersService sharedInstance];
    OWIdentificationService *identification = [OWIdentificationService sharedInstance];
    NSLog(@"Retrieving Offers");
    [hasOffers retrieveAllOffersForAPIKey:apiKey Country:[identification getCountryCode] Device:[identification getDeviceTypeName] AdvertisingIdentifier:[identification getAdvertisingIdentifier]  Success:^(NSArray *results) {
        NSLog(@"Retrieved Offer Information");
        successCallback(results);
    } Failure:^(NSError *error) {
        failureCallback(error);
    }];
}

@end
