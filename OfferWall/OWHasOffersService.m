//
//  OWHasOffersService.m
//  OfferWall
//
//  Created by Sharim Chua on 5/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OWHasOffersService.h"
#import "OWOffer.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

#define OW_API_URL @"http://offerwallapi.brusmedia.com/api/v1/offers/byCountryDevice"

@implementation OWHasOffersService

+(OWHasOffersService *)sharedInstance {
    static OWHasOffersService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[OWHasOffersService alloc] init];
    });
    return sharedInstance;
}

-(void)retrieveAllOffersForAPIKey:(NSString *)apiKey Country:(NSString *)countryCode Device:(NSString *)device AdvertisingIdentifier: (NSString *)ifa Success:(void (^)(NSArray *))successCallback Failure:(void (^)(NSError *))failureCallback {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", nil];
    [manager POST:OW_API_URL
       parameters:@{
                    @"countryCode": countryCode,
                    @"device": device,
                    @"apiKey": apiKey,
                    @"deviceId": ifa
                    }
constructingBodyWithBlock:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSMutableArray __block *offers = [[NSMutableArray alloc] init];
              NSArray *data = [[responseObject objectForKey:@"data"] objectForKey:@"offers"];
              [data enumerateObjectsUsingBlock:^(NSDictionary *offerData, NSUInteger idx, BOOL *stop) {
                  OWOffer *offer = [[OWOffer alloc] init];
                  offer.offerId = [[offerData objectForKey:@"offerId"] intValue];
                  offer.description = [offerData objectForKey:@"name"];
                  offer.price = [[offerData objectForKey:@"price"] integerValue];
                  offer.thumbnailUrl = [offerData objectForKey:@"thumbnailUrl"];
                  offer.trackingLinkUrl = [offerData objectForKey:@"trackingLinkUrl"];
                  [offers addObject:offer];
              }];
              successCallback(offers);
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureCallback(error);
    }];
}

@end
