//
//  OfferWall.m
//  OfferWall
//
//  Created by Sharim Chua on 3/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OfferWall.h"
#import "OWOFferDataSource.h"
#import "OWIdentificationService.h"
#import "OWOfferWallViewController.h"
#import "OWConfiguration.h"

@interface OfferWall ()
@property (strong, nonatomic) OWConfiguration *configuration;
@property (strong, nonatomic) OWOfferDataSource *dataSource;
@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) NSTimeInterval timerInterval;
@property (assign, nonatomic) NSTimeInterval remainingTimeInterval;
@property (weak, nonatomic) UIViewController *timerViewController;
+(OfferWall *) getInstance;
-(void) displayTimedOfferWall;
-(void) displayOfferWallOnViewController:(UIViewController *)viewController;
-(void) loadOffers;
@end

@implementation OfferWall

+(OfferWall *)getInstance {
    static OfferWall *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[OfferWall alloc] init];
    });
    return sharedInstance;
}

+(void)initialiseOfferWallWithKey:(NSString *)apiKey {
    OfferWall *instance = [OfferWall getInstance];
    instance.configuration = [[OWConfiguration alloc] init];
    instance.configuration.apiKey = apiKey;
    instance.dataSource = [[OWOfferDataSource alloc] initWithConfiguration: instance.configuration];
    [instance loadOffers];
}

+(void)setConfiguration:(NSDictionary *)configurationSettings {
    if ([configurationSettings valueForKey:kCellViewConfig] != nil) {
        [OfferWall setCellView:[configurationSettings objectForKey:kCellViewConfig]];
    }
    if ([configurationSettings valueForKey:kLoadingViewConfig] != nil) {
        [OfferWall setLoadingView:[configurationSettings objectForKey:kLoadingViewConfig]];
    }
    if ([configurationSettings valueForKey:kStatusBarVisibilityConfig] != nil) {
        [OfferWall setStatusBarVisibility:[[configurationSettings objectForKey:kStatusBarVisibilityConfig] boolValue]];
    } else {
        [OfferWall setStatusBarVisibility:YES];
    }
    if ([configurationSettings valueForKey:kBackgroundColourConfig] != nil) {
        [OfferWall setBackgroundColour:[configurationSettings objectForKey:kBackgroundColourConfig]];
    }
    if ([configurationSettings valueForKey:kHorizontalPaddingConfig] != nil) {
        [OfferWall setHorizontalPadding:[[configurationSettings objectForKey:kHorizontalPaddingConfig] doubleValue]];
    }
    if ([configurationSettings valueForKey:kCustomHeaderViewConfig] != nil) {
        [OfferWall setCustomHeaderView:[configurationSettings objectForKey:kCustomHeaderViewConfig]];
    }
    if ([configurationSettings valueForKey:kCloseButtonImageConfig] != nil) {
        [OfferWall setCloseButtonImage:[configurationSettings objectForKey:kCloseButtonImageConfig]];
    }
}

+(void)setCellView:(UINib *)cellViewNib {
    [OfferWall getInstance].dataSource.cellViewNib = cellViewNib;
}

+(void)setLoadingView:(UINib *)loadingViewNib {
    [OfferWall getInstance].configuration.loadingViewNib = loadingViewNib;
}

+(void)setBackgroundColour:(UIColor *)backgroundColour {
    [OfferWall getInstance].configuration.backgroundColour = backgroundColour;
}

+(void)setStatusBarVisibility:(BOOL)visibility {
    [OfferWall getInstance].configuration.statusBarVisibility = visibility;
}

+(void)setCloseButtonImage:(UIImage *)closeButtonImage {
    [OfferWall getInstance].configuration.closeImage = closeButtonImage;
}

+(void)setHorizontalPadding:(int)padding {
    [OfferWall getInstance].configuration.horizontalPadding = padding;
}

+(void)setCustomHeaderView:(UINib *)headerViewNib {
    [OfferWall getInstance].configuration.headerViewNib = headerViewNib;
}

+(void)showOffersFromViewController:(UIViewController *)viewController {
    OfferWall *instance = [OfferWall getInstance];
    [instance displayOfferWallOnViewController:viewController];
}

+(void)startOfferWallTimer:(NSTimeInterval)interval viewController:(UIViewController *)viewController {
    OfferWall *instance = [OfferWall getInstance];
    instance.timerViewController = viewController;
    instance.timerInterval = interval;
    instance.remainingTimeInterval = 0;
    instance.timer = [NSTimer scheduledTimerWithTimeInterval:interval target:instance selector:@selector(displayTimedOfferWall) userInfo:nil repeats:YES];
}

+(void)stopOfferWallTimer {
    OfferWall *instance = [OfferWall getInstance];
    [instance.timer invalidate];
    instance.timerViewController = nil;
    instance.timerInterval = 0;
    instance.remainingTimeInterval = 0;
}

+(void)pauseOfferWallTimer {
    OfferWall *instance = [OfferWall getInstance];
    if (instance.timer.isValid && instance.remainingTimeInterval == 0) {
        instance.remainingTimeInterval = [instance.timer.fireDate timeIntervalSinceNow];
        [instance.timer setFireDate:[NSDate distantFuture]];
    }
}

+(void)restartOfferWallTimer {
    OfferWall *instance = [OfferWall getInstance];
    if (instance.timerViewController && instance.timerInterval > 0) {
        [instance.timer setFireDate:[NSDate dateWithTimeIntervalSinceNow:instance.timerInterval]];
        instance.remainingTimeInterval = 0;
    }
}

+(void)resumeOfferWallTimer {
    OfferWall *instance = [OfferWall getInstance];
    if (instance.timerViewController && instance.remainingTimeInterval > 0) {
        [instance.timer setFireDate:[NSDate dateWithTimeIntervalSinceNow:instance.remainingTimeInterval]];
        instance.remainingTimeInterval = 0;
    }
}

-(void)displayTimedOfferWall {
    // Check that the view controller is valid and in view. If not stop the timer.
    if (self.timerViewController.isViewLoaded && self.timerViewController.view.window) {
        // Only show the OfferWall if the offers have been loaded by now
        if ([self.dataSource dataLoadComplete]) {
            [self displayOfferWallOnViewController:self.timerViewController];
        }
    } else {
        [OfferWall stopOfferWallTimer];
    }
}

-(void)displayOfferWallOnViewController:(UIViewController *)viewController {
    if (viewController) {
        OWOfferWallViewController *controller = [[OWOfferWallViewController alloc] initWithTableManager:self.dataSource Configuration: self.configuration];
        [viewController presentViewController:controller animated:YES completion:nil];
    }
}

-(void)loadOffers {
    [self.dataSource loadData];
}

@end
