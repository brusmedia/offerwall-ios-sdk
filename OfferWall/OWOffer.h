//
//  OWOffer.h
//  OfferWall
//
//  Created by Sharim Chua on 3/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OWOffer : NSObject
@property (assign, nonatomic) int offerId;
@property (assign, nonatomic) int affiliateId;
@property (assign, nonatomic) double price;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSString *thumbnailUrl;
@property (strong, nonatomic) NSString *trackingLinkUrl;

-(NSString *)priceLabel;
-(NSString *)nameLabel;
@end
