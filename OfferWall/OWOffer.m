//
//  OWOffer.m
//  OfferWall
//
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OWOffer.h"

@implementation OWOffer

-(NSString *)nameLabel {
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@" \\(.*\\)" options:NSRegularExpressionCaseInsensitive error:nil];
    return [regex stringByReplacingMatchesInString:self.description options:kNilOptions range:NSMakeRange(0, [self.description length]) withTemplate:@""];
}

-(NSString *)priceLabel {
    if (self.price > 0) {
        return [NSString stringWithFormat:@"%1.2f", self.price];
    } else {
        return @"FREE";
    }
}

@end
