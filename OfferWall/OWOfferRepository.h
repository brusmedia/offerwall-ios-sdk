//
//  OWOfferRepository.h
//  OfferWall
//
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OWOfferRepository : NSObject
-(void)retrieveOffersForApiKey:(NSString *)apiKey
                       Success:(void(^)(NSArray *results))successCallback
                       Failure:(void(^)(NSError *error))failureCallback;
@end
