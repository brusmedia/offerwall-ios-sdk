//
//  OfferWallViewController.m
//  OfferWall
//
//  Created by Sharim Chua on 6/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OWOfferWallViewController.h"
#import "OWLoadingViewController.h"
#import "OWStaticAssets.h"
#import "OfferWall.h"

@interface OWOfferWallViewController ()
@property (weak, nonatomic) OWOfferDataSource *tableManager;
@property (weak, nonatomic) OWConfiguration *configuration;
@property (nonatomic, strong) OWLoadingViewController *loadingViewController;
@property (nonatomic, strong) UITableView *offerTable;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, strong) UILabel *headerTitle;
-(void)dismissOfferWall;
-(void)setupOfferWallView;
-(void)setupHeaderView;
-(void)setupBasicHeader;
@end

@implementation OWOfferWallViewController

-(id)initWithTableManager:(id<UITableViewDelegate, UITableViewDataSource>) tableManager
            Configuration:(OWConfiguration *) configuration {
    self = [super init];
    if (self) {
        self.tableManager = tableManager;
        self.tableManager.delegate = self;
        self.configuration = configuration;
        self.loadingViewController = [[OWLoadingViewController alloc] initWithConfiguration: configuration];
    }
    return self;
}

-(BOOL)prefersStatusBarHidden {
    return !self.configuration.statusBarVisibility;
}

-(void)setupBasicHeader {
    self.headerView = [[UIView alloc] init];
    self.closeButton = [[UIButton alloc] init];
    [self.closeButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.headerView addSubview:self.closeButton];
}

-(void)setupHeaderView {
    if (self.configuration.headerViewNib) {
        // Preference custom header view if one is provided
        OWOfferTableHeader *header = [[self.configuration.headerViewNib instantiateWithOwner:self options:nil] lastObject];
        self.closeButton = header.closeButton;
        self.headerView = header;
    } else if (self.configuration.closeImage) {
        // Otherwise use the basic close button image if one is provided
        [self setupBasicHeader];
        NSArray *verticalCloseButtonContraints = [NSLayoutConstraint
                                                  constraintsWithVisualFormat:@"V:|-5-[closeButton]-15-|"
                                                  options:NSLayoutFormatDirectionLeadingToTrailing
                                                  metrics:nil
                                                  views:@{@"closeButton": self.closeButton}];
        
        NSArray *horizontalCloseButtonContraints = [NSLayoutConstraint
                                                    constraintsWithVisualFormat:@"[closeButton]-0-|"
                                                    options:NSLayoutFormatDirectionLeadingToTrailing
                                                    metrics:nil
                                                    views:@{@"closeButton": self.closeButton}];
        [self.headerView addConstraints:verticalCloseButtonContraints];
        [self.headerView addConstraints:horizontalCloseButtonContraints];
        [self.closeButton setImage:self.configuration.closeImage forState:UIControlStateNormal];
    } else {
        // Fall back to default header view
        [self setupBasicHeader];
        [self.closeButton setImage:[OWStaticAssets defaultCloseButtonImage] forState:UIControlStateNormal];
        self.headerTitle = [[UILabel alloc] init];
        self.headerTitle.text = @"More Free Games";
        self.headerTitle.textColor = [UIColor whiteColor];
        self.headerTitle.backgroundColor = [UIColor clearColor];
        [self.headerTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.headerView addSubview:self.headerTitle];
        NSArray *verticalTitleConstraints = [NSLayoutConstraint
                                             constraintsWithVisualFormat:@"V:|[title]|"
                                             options:NSLayoutFormatDirectionLeadingToTrailing
                                             metrics:nil
                                             views:@{@"title": self.headerTitle}];
        NSArray *verticalCloseButtonContraints = [NSLayoutConstraint
                                                  constraintsWithVisualFormat:@"V:|[closeButton]|"
                                                  options:NSLayoutFormatDirectionLeadingToTrailing
                                                  metrics:nil
                                                  views:@{@"closeButton": self.closeButton}];
        
        NSArray *horizontalContraints = [NSLayoutConstraint
                                                    constraintsWithVisualFormat:@"|-20-[title]-10@1-[closeButton]-0-|"
                                                    options:NSLayoutFormatDirectionLeadingToTrailing
                                                    metrics:nil
                                                    views:@{@"closeButton": self.closeButton, @"title": self.headerTitle}];
        [self.headerView addConstraints:verticalCloseButtonContraints];
        [self.headerView addConstraints:verticalTitleConstraints];
        [self.headerView addConstraints:horizontalContraints];
        self.headerView.backgroundColor = [UIColor colorWithPatternImage:[OWStaticAssets defaultHeaderGradientImage]];
    }
    [self.headerView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.closeButton addTarget:self action:@selector(dismissOfferWall) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setupOfferWallView {
    self.mainView = [[UIView alloc] initWithFrame:CGRectZero];
    if (self.configuration.backgroundColour) {
        self.mainView.backgroundColor = self.configuration.backgroundColour;
    } else {
        self.mainView.backgroundColor = [UIColor blackColor];
    }
    
    self.offerTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.offerTable.dataSource = self.tableManager;
    self.offerTable.delegate = self.tableManager;
    [self.offerTable setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self setupHeaderView];
    
    [self.mainView addSubview:self.headerView];
    [self.mainView addSubview:self.offerTable];
    
    NSDictionary *viewsDictionary = @{@"headerView": self.headerView, @"offerTable": self.offerTable};
    CGFloat headerHeight = self.headerView.frame.size.height;
    NSString *heightConstraint = @"";
    if (headerHeight > 0) {
        heightConstraint = [NSString stringWithFormat:@"(==%1.0f)", headerHeight];
    }
    NSString *verticalFormat = [NSString stringWithFormat:@"V:|-20-[headerView%@][offerTable(>=100)]-20-|", heightConstraint];
    NSArray *verticalConstraints = [NSLayoutConstraint
                                    constraintsWithVisualFormat: verticalFormat
                                    options:NSLayoutFormatDirectionLeadingToTrailing
                                    metrics:nil
                                    views:viewsDictionary];
    NSString *horizontalFormat = @"|-[view]-|";
    if (self.configuration.horizontalPadding > 0) {
        horizontalFormat = [NSString stringWithFormat:@"|-%d-[view]-%d-|", self.configuration.horizontalPadding, self.configuration.horizontalPadding];
    }
    NSArray *horizontalTableConstraints = [NSLayoutConstraint
                                           constraintsWithVisualFormat: horizontalFormat
                                           options:NSLayoutFormatDirectionLeadingToTrailing
                                           metrics:nil
                                           views:@{@"view": self.offerTable}];
    NSArray *horizontalHeaderConstraints = [NSLayoutConstraint
                                            constraintsWithVisualFormat: horizontalFormat
                                            options:NSLayoutFormatDirectionLeadingToTrailing
                                            metrics:nil
                                            views:@{@"view": self.headerView}];
    
    [self.mainView addConstraints: verticalConstraints];
    [self.mainView addConstraints: horizontalTableConstraints];
    [self.mainView addConstraints: horizontalHeaderConstraints];
}

-(void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated {
    if (![self.tableManager dataLoadComplete]) {
        self.loadingViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:self.loadingViewController animated:YES completion:nil];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    
    [self revealOfferWall];
}

-(void)revealOfferWall {
    BOOL loadComplete = [self.tableManager dataLoadComplete];
    if (loadComplete) {
        [self.offerTable reloadData];
    }
    [self.view.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        subview.hidden = !loadComplete;
    }];
    if (!loadComplete) {
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(revealOfferWall) userInfo:nil repeats:NO];
    }
}

-(void)loadView {
    [self setupOfferWallView];
    self.view = self.mainView;
}

-(void)dismissOfferWall {    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark OWOfferDataSourceDelegate

-(void)hasSelectedOffer {
    [self dismissOfferWall];
}

@end
