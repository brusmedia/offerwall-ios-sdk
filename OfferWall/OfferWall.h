//
//  OfferWall.h
//  OfferWall
//
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

/* Key definitions for configuration settings dictionary */
#define kCellViewConfig @"CellView"
#define kLoadingViewConfig @"LoadingView"
#define kBackgroundColourConfig @"BackgroundColour"
#define kStatusBarVisibilityConfig @"StatusBarVisibility"
#define kCloseButtonImageConfig @"CloseButtonImage"
#define kHorizontalPaddingConfig @"HorizontalPadding"
#define kCustomHeaderViewConfig @"CustomHeaderView"

// Header view on top of Offer Wall
@interface OWOfferTableHeader : UIView
// Close button to dismiss offer wall
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@end

// UITableView cell for Offer Wall
@interface OWOfferCell : UITableViewCell
// Thumbnail image of offer
@property (weak, nonatomic) IBOutlet UIImageView *offerImage;
// Price of offer
@property (weak, nonatomic) IBOutlet UILabel *offerPrice;
// Description of offer
@property (weak, nonatomic) IBOutlet UILabel *offerDescription;
@end

@interface OfferWall : NSObject

// Initialise the Offer Wall. Should be called when the application is loaded.
// API Key of the application should be passed in
+(void) initialiseOfferWallWithKey:(NSString *) apiKey;

/* Configuration */

// Set multiple configuration items at once with a dictionary
+(void) setConfiguration:(NSDictionary *) configurationSettings;

// Set view to show when if offers are still being loaded. NIB should only contain one view which is the loading view
+(void) setLoadingView:(UINib *) loadingViewNib;

// Set background colour of the Offer Wall
+(void) setBackgroundColour:(UIColor *) backgroundColour;

// Set expected status bar visibility (defaults to YES)
+(void) setStatusBarVisibility:(BOOL) visibility;

// Set the close button image of the Offer Wall (assuming a custom header isn't used)
+(void) setCloseButtonImage:(UIImage *) closeButtonImage;

// Set the cell view for individual offers on the Offer Wall. NIB should only contain one view which is the cell view
// The cell view should be an instance of OWOfferCell with controls linked to the IBOutlets
+(void) setCellView:(UINib *) cellViewNib;

// Customise horizontal padding of Offer Wall
+(void) setHorizontalPadding:(int) padding;

// Override table header with custom view. NIB should only contain one view which is the header.
// The header view should be an instance of OWOfferTableHeader with controls linked to the IBOutlets
+(void) setCustomHeaderView:(UINib *) headerViewNib;

/* Display Offer Wall */

// Showcase offers in a modal table with the branding cells specified
+(void) showOffersFromViewController:(UIViewController *) viewController;

// Starts a timer for the Offer Wall to display repeatedly at a specific interval
+(void) startOfferWallTimer:(NSTimeInterval)interval viewController:(UIViewController *) viewController;

// Restarts a timer with the original interval set
+(void) restartOfferWallTimer;

// Pauses the Offer Wall Timer
+(void) pauseOfferWallTimer;

// Resume the Offer Wall Timer
+(void) resumeOfferWallTimer;

// Stops the timer for Offer Wall display. Should be called when leaving the current view controller where timer was started
+(void) stopOfferWallTimer;

@end
