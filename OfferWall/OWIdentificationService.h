//
//  OWIdentificationService.h
//  OfferWall
//
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OWIdentificationService : NSObject
-(NSString *)getCountryCode;
-(NSString *)getAdvertisingIdentifier;
-(NSString *)getDeviceTypeName;
+(OWIdentificationService *)sharedInstance;
@end
