//
//  OWOfferDataSource.m
//  OfferWall
//
//  Created by Sharim Chua on 3/02/2014.
//  Copyright (c) 2014 Brusmedia Pty Ltd. All rights reserved.
//

#import "OWOfferDataSource.h"
#import "OWOfferRepository.h"
#import "OfferWall.h"
#import "OWOffer.h"
#import "OWStaticAssets.h"
#import <UIImageView+AFNetworking.h>

@interface OWOfferDataSource ()
@property (strong, nonatomic) OWOfferRepository *repository;
@property (strong, nonatomic) NSArray *offerData;
@property (strong, nonatomic) OWConfiguration *configuration;
@property (assign, nonatomic) BOOL loadingData;
-(OWOfferCell *) getInstanceOfCellView;
-(void)finaliseDataLoad;
@end

@implementation OWOfferDataSource

-(id)initWithConfiguration: (OWConfiguration *) configuration {
    self = [super init];
    if (self) {
        self.repository = [[OWOfferRepository alloc] init];
        self.configuration = configuration;
    }
    return self;
}

-(void)finaliseDataLoad {
    self.loadingData = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:kOffersLoadedEvent object:self];
}

-(void)loadData {
    if (self.loadingData) return;
    self.loadingData = YES;
    [self.repository retrieveOffersForApiKey: self.configuration.apiKey Success:^(NSArray *results) {
        self.offerData = results;
        [self finaliseDataLoad];
        NSLog(@"Offers have been loaded");
    } Failure:^(NSError *error) {
        self.offerData = [[NSArray alloc] init];
        NSLog(@"Offers could not be loaded: %@", error);
    }];
}

-(BOOL)dataLoadComplete {
    return self.loadingData == NO;
}

-(OWOfferCell *)getInstanceOfCellView {
    if (self.cellViewNib) {
        return (OWOfferCell *) [[self.cellViewNib instantiateWithOwner:self options:nil] lastObject];
    } else {
        OWOfferCell *cell = [[OWOfferCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OfferCell"];
        cell.frame = CGRectMake(0,0, 300, 60);
        cell.contentView.backgroundColor = [UIColor colorWithPatternImage:[OWStaticAssets defaultCellGradientImage]];
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.translatesAutoresizingMaskIntoConstraints = NO;
        [cell.contentView addSubview:imageView];
        cell.offerImage = imageView;
        
        UILabel *nameLabel = [[UILabel alloc] init];
        nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        nameLabel.numberOfLines = 0;
        nameLabel.lineBreakMode = NSLineBreakByWordWrapping;
        nameLabel.font = [UIFont systemFontOfSize:14.0];
        nameLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:nameLabel];
        cell.offerDescription = nameLabel;
        
        UIImageView *priceBackground = [[UIImageView alloc] init];
        priceBackground.translatesAutoresizingMaskIntoConstraints = NO;
        priceBackground.image = [OWStaticAssets defaultPriceButtonImage];
        [cell.contentView addSubview:priceBackground];
        
        UILabel *priceLabel = [[UILabel alloc] init];
        priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        priceLabel.textColor = [UIColor whiteColor];
        priceLabel.font = [UIFont systemFontOfSize:12.0];
        priceLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:priceLabel];
        cell.offerPrice = priceLabel;
        
        NSArray *verticalImageContraints = [NSLayoutConstraint
                                                  constraintsWithVisualFormat:@"V:|-5-[image]-5-|"
                                                  options:NSLayoutFormatDirectionLeadingToTrailing
                                                  metrics:nil
                                                  views:@{@"image": imageView}];
        NSArray *verticalNameContraints = [NSLayoutConstraint
                                            constraintsWithVisualFormat:@"V:|-5-[name]-5-|"
                                            options:NSLayoutFormatDirectionLeadingToTrailing
                                            metrics:nil
                                            views:@{@"name": nameLabel}];
        NSArray *verticalPriceButtonContraints = [NSLayoutConstraint
                                           constraintsWithVisualFormat:@"V:|-18-[priceButton]-18-|"
                                           options:NSLayoutFormatDirectionLeadingToTrailing
                                           metrics:nil
                                           views:@{@"priceButton": priceBackground}];
        NSArray *horizontalConstraints = [NSLayoutConstraint
                                                    constraintsWithVisualFormat:@"|-5-[image(50)]-10-[name]-5-[priceButton(50)]-5-|"
                                                    options:NSLayoutFormatDirectionLeadingToTrailing
                                                    metrics:nil
                                                    views:@{@"image": imageView, @"name": nameLabel, @"priceButton": priceBackground}];
        [cell addConstraints:verticalImageContraints];
        [cell addConstraints:verticalNameContraints];
        [cell addConstraints:verticalPriceButtonContraints];
        [cell addConstraints:horizontalConstraints];
        [cell addConstraint:[NSLayoutConstraint constraintWithItem:priceLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:priceBackground attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        [cell addConstraint:[NSLayoutConstraint constraintWithItem:priceLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:priceBackground attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
        
        return cell;
    }
}

#pragma mark UITableViewDelegate methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self getInstanceOfCellView].frame.size.height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    OWOffer *offer = [self.offerData objectAtIndex:indexPath.row];
    NSURL *url = [NSURL URLWithString:offer.trackingLinkUrl];
    [[UIApplication sharedApplication] openURL:url];
    [self.delegate hasSelectedOffer];
}

#pragma mark UITableViewDataSource methods

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.cellViewNib) {
        [tableView registerNib:self.cellViewNib forCellReuseIdentifier:@"OfferCell"];
    }
    OWOfferCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OfferCell"];
    if (!cell) {
        cell = [self getInstanceOfCellView];
    }
    OWOffer *offer = [self.offerData objectAtIndex:indexPath.row];
    cell.offerPrice.text = [offer priceLabel];
    cell.offerDescription.text = [offer nameLabel];
    [cell.offerImage setImageWithURL:[NSURL URLWithString:offer.thumbnailUrl]];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.offerData == nil) {
        [self loadData];
        return 0;
    } else {
        return [self.offerData count];
    }
}

@end
